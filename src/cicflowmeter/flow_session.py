from collections import defaultdict
import pika
import json
import logging

from scapy.sessions import DefaultSession

from .features.context.packet_direction import PacketDirection
from .features.context.packet_flow_key import get_packet_flow_key
from .flow import Flow

EXPIRED_UPDATE = 40
GARBAGE_COLLECT_PACKETS = 100


class FlowSession(DefaultSession):
    """Creates a list of network flows."""

    def __init__(self, *args, **kwargs):
        self.flows = {}

        self.packets_count = 0

        if self.rabbitmq_host is not None and self.rabbitmq_port is not None:
            self.rabbitmq = True
            self.connection = pika.BlockingConnection(pika.ConnectionParameters(
                host=self.rabbitmq_host, 
                port=self.rabbitmq_port,
            ))
            self.channel = self.connection.channel()
            self.channel.queue_declare(queue='classifier')

        self.clumped_flows_per_label = defaultdict(list)

        super(FlowSession, self).__init__(*args, **kwargs)

    def toPacketList(self):
        # Sniffer finished all the packets it needed to sniff.
        # It is not a good place for this, we need to somehow define a finish signal for AsyncSniffer
        self.garbage_collect(None)
        return super(FlowSession, self).toPacketList()

    def on_packet_received(self, packet):
        global GARBAGE_COLLECT_PACKETS
        global EXPIRED_UPDATE

        count = 0
        direction = PacketDirection.FORWARD

        if self.output_mode != "flow":
            if "TCP" not in packet:
                return
            elif "UDP" not in packet:
                return

        try:
            # Creates a key variable to check
            packet_flow_key = get_packet_flow_key(packet, direction)
            flow = self.flows.get((packet_flow_key, count))
        except Exception:
            return

        self.packets_count += 1

        # If there is no forward flow with a count of 0
        if flow is None:
            # There might be one of it in reverse
            direction = PacketDirection.REVERSE
            packet_flow_key = get_packet_flow_key(packet, direction)
            flow = self.flows.get((packet_flow_key, count))

        if flow is None:
            # If no flow exists create a new flow
            direction = PacketDirection.FORWARD
            flow = Flow(packet, direction)
            packet_flow_key = get_packet_flow_key(packet, direction)
            self.flows[(packet_flow_key, count)] = flow

        elif (packet.time - flow.latest_timestamp) > EXPIRED_UPDATE:
            # If the packet exists in the flow but the packet is sent
            # after too much of a delay than it is a part of a new flow.
            expired = EXPIRED_UPDATE
            while (packet.time - flow.latest_timestamp) > expired:
                count += 1
                expired += EXPIRED_UPDATE
                flow = self.flows.get((packet_flow_key, count))

                if flow is None:
                    flow = Flow(packet, direction)
                    self.flows[(packet_flow_key, count)] = flow
                    break
        # elif "F" in str(packet.flags):
        #     # If it has FIN flag then early collect flow and continue
        #     flow.add_packet(packet, direction)
        #     self.garbage_collect(packet.time)
        #     return

        flow.add_packet(packet, direction)

        if self.packets_count % GARBAGE_COLLECT_PACKETS == 0 or (
            flow.duration > 120 and self.output_mode == "flow"
        ):
            self.garbage_collect(packet.time)

    def get_flows(self) -> list:
        return self.flows.values()

    def garbage_collect(self, latest_time) -> None:
        # TODO: Garbage Collection / Feature Extraction should have a separate thread
        logging.info(f"Garbage Collection began. Flows = {len(self.flows)}")
        keys = list(self.flows.keys())
        for k in keys:
            flow = self.flows.get(k)

            if (
                latest_time is None
                or latest_time - flow.latest_timestamp > EXPIRED_UPDATE
                or flow.duration > 90
            ):
                data = flow.get_data()

                if self.rabbitmq:
                    # data_to_send = json.dumps({
                    #     "columns": list(data.keys()),
                    #     "data": [list(data.values())],
                    # })
                    self.channel.basic_publish(exchange='', routing_key='classifier', body=json.dumps(data))
                    logging.info("Data send to RabbitMQ")

                del self.flows[k]
        logging.info(f"Garbage Collection finished. Flows = {len(self.flows)}")


def generate_session_class(output_mode: str, rabbitmq_host: str, rabbitmq_port: int):
    return type(
        "NewFlowSession",
        (FlowSession,),
        {
            "output_mode": output_mode,
            "rabbitmq_host": rabbitmq_host,
            "rabbitmq_port": rabbitmq_port,
        },
    )
